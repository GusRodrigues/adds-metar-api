package com.gustavo.rodrigues.aviation.metarAPI.domain;

public class Metar {
	private final String raw;
	private static final Metar EMPTY_METAR = new EmptyMetar();

	private Metar(String metar) {
		this.raw = metar.toUpperCase();
		checkRep();
	}

	private void checkRep() {
		if (this.raw == null)
			throw new NullPointerException();
		if (this.raw.trim().isEmpty())
			throw new NullPointerException();
	}

	public static Metar empty() {
		return EMPTY_METAR;
	}

	public static Metar raw(String metar) {
		return new Metar(metar);
	}

	public String getRawText() {
		return this.raw;
	}

	@Override
	public String toString() {
		return this.raw;
	}

	public boolean isEmpty() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + raw.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Metar other = (Metar) obj;
		if (!raw.equalsIgnoreCase(other.raw))
			return false;
		return true;
	}
	
	public String reportingStation() {
		if (!this.isEmpty()) {
			return raw.substring(0,4).trim().toUpperCase();
		}
		return "no one is reporting";
	}
	
	/**
	 * Returns an empty representation of the Metar class
	 * @author Gustavo Rodrigues
	 *
	 */
	private static class EmptyMetar extends Metar {
		protected EmptyMetar() {
			super("empty".toUpperCase());
		}
		@Override
			public boolean isEmpty() {
				return true;
			}
	}
}
