//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.07.29 às 05:09:49 PM EDT 
//


package com.gustavo.rodrigues.aviation.metarAPI.domain.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="sky_cover" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cloud_base_ft_agl" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "sky_condition")
public class SkyCondition {

    @XmlAttribute(name = "sky_cover")
    protected String skyCover;
    @XmlAttribute(name = "cloud_base_ft_agl")
    protected Integer cloudBaseFtAgl;

    /**
     * Obtém o valor da propriedade skyCover.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkyCover() {
        return skyCover;
    }

    /**
     * Define o valor da propriedade skyCover.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkyCover(String value) {
        this.skyCover = value;
    }

    /**
     * Obtém o valor da propriedade cloudBaseFtAgl.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCloudBaseFtAgl() {
        return cloudBaseFtAgl;
    }

    /**
     * Define o valor da propriedade cloudBaseFtAgl.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCloudBaseFtAgl(Integer value) {
        this.cloudBaseFtAgl = value;
    }

}
