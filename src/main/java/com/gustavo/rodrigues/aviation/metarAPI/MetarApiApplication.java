package com.gustavo.rodrigues.aviation.metarAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetarApiApplication.class, args);
	}

}
