package com.gustavo.rodrigues.aviation.metarAPI.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.rodrigues.aviation.metarAPI.domain.Metar;
import com.gustavo.rodrigues.aviation.metarAPI.service.AviationWeatherMetarService;

@RestController
@RequestMapping("/metar")
public class MetarController {
	@Autowired
	AviationWeatherMetarService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/airport")
	public Map<String, String> queryOneAirportForRecentMetar(@RequestParam String icao) {
		Metar metar = service.queryMostRecentMetar(icao);
		return Collections.singletonMap(metar.reportingStation(), metar.toString());
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/airports")
	public Map<String, String> queryMultipleAirportsForRecenteMetar(@RequestParam() List<String> icao) {
		List<Metar> listOfMetar = service.queryMostRecentMultiAirport(icao);
		return listOfMetar.stream().collect(
                Collectors.toMap(Metar::reportingStation, Metar::getRawText));
	}
}
