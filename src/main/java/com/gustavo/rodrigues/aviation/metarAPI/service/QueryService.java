package com.gustavo.rodrigues.aviation.metarAPI.service;

import java.util.List;

import com.gustavo.rodrigues.aviation.metarAPI.domain.QueryBuilder;

public class QueryService {
	private final static QueryBuilder API = QueryBuilder.API_URL;
	private final static QueryBuilder mostRecentOnly = QueryBuilder.MOST_RECENT_ONLY;
	private final static QueryBuilder mostRecent = QueryBuilder.MOST_RECENT_TIME;
	private final static QueryBuilder stations = QueryBuilder.STATIONS_TO_QUERY;
	private final static QueryBuilder timeBefore = QueryBuilder.TIME_BEFORE;

	/**
	 * Returns only the first METARs/SPECIs from the last valid hour.
	 * 
	 * @param station representing the airport 4 character ICAO Code. Cannot be
	 *                null.
	 * @return the String to query ADDS for the most recent METAR of that Location
	 */
	public static String RECENT_ONLY_QUERY(String station) {
		StringBuilder sb = new StringBuilder();
		sb.append(API);
		sb.append(mostRecentOnly);
		sb.append(stations);
		sb.append(station);
		return sb.toString();
	}

	/**
	 * Returns All the METARs/SPECI from the last valid hour from now.
	 * 
	 * @param station representing the airport 4 character ICAO Code. Cannot be
	 *                null.
	 * @return the String to query ADDS for the most recent METAR of that Location
	 */
	public static String ALL_RECENT_ONLY(String station) {
		StringBuilder sb = new StringBuilder();
		sb.append(API);
		sb.append(mostRecent);
		sb.append(stations);
		sb.append(station);
		return sb.toString();
	}

	public static String RECENT_WEATHER_MULTI_STATION(List<String> stationList) {
		StringBuilder sb = new StringBuilder();
		sb.append(API);
		sb.append(mostRecentOnly);
		sb.append(stations);
		stationList.forEach((airport) -> {
			sb.append(airport + ",");
		});
		return sb.toString();
	}
}
