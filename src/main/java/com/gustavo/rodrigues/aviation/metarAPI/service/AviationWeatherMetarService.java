package com.gustavo.rodrigues.aviation.metarAPI.service;

import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gustavo.rodrigues.aviation.metarAPI.domain.Metar;
import com.gustavo.rodrigues.aviation.metarAPI.domain.generated.Response;

/**
 * It will connect to the AviationWeather API Retrieve the required Payload (XML
 * file with METAR for given ICAO) Return a JSON with the <raw_text> only
 * 
 * @author Gustavo Rodrigues
 *
 */
@Service
public class AviationWeatherMetarService {
	/**
	 * Querys ADDS service for the METAR of that unique Station. Service can only
	 * support one station
	 * 
	 * @param icaoCode
	 * @return the Metar of that station
	 */
	public Metar queryMostRecentMetar(String icaoCode) {
		String url = QueryService.RECENT_ONLY_QUERY(icaoCode);
		return fromADDSXmlToMetar(url);
	}
	
	public List<Metar> queryMostRecentMultiAirport(List<String> aiports) {
		String url = QueryService.RECENT_WEATHER_MULTI_STATION(aiports);
		return fromADDSXmlToList(url);
	}

	private Metar fromADDSXmlToMetar(String url) {
		XmlMapper mapper = new XmlMapper();
		try (InputStream stream = new URL(url).openStream()) {
			JsonNode node = mapper.readTree(stream);
			return Metar.raw(node.findValue("raw_text").asText());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Metar.empty();
	}

	public List<Metar> fromADDSXmlToList(String url) {
		try (InputStream stream = new URL(url).openStream()) {
			JAXBContext context = JAXBContext.newInstance(Response.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Response response = (Response) unmarshaller.unmarshal(stream);

			return response.getData().getMETAR().stream().map((m) -> Metar.raw(m.getRawText()))
					.collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Collections.emptyList();
	}
}
