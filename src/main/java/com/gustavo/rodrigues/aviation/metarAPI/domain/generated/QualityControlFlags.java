//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.07.29 às 05:09:49 PM EDT 
//


package com.gustavo.rodrigues.aviation.metarAPI.domain.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}corrected" minOccurs="0"/>
 *         &lt;element ref="{}auto" minOccurs="0"/>
 *         &lt;element ref="{}auto_station" minOccurs="0"/>
 *         &lt;element ref="{}maintenance_indicator_on" minOccurs="0"/>
 *         &lt;element ref="{}no_signal" minOccurs="0"/>
 *         &lt;element ref="{}lightning_sensor_off" minOccurs="0"/>
 *         &lt;element ref="{}freezing_rain_sensor_off" minOccurs="0"/>
 *         &lt;element ref="{}present_weather_sensor_off" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corrected",
    "auto",
    "autoStation",
    "maintenanceIndicatorOn",
    "noSignal",
    "lightningSensorOff",
    "freezingRainSensorOff",
    "presentWeatherSensorOff"
})
@XmlRootElement(name = "quality_control_flags")
public class QualityControlFlags {

    protected String corrected;
    protected String auto;
    @XmlElement(name = "auto_station")
    protected String autoStation;
    @XmlElement(name = "maintenance_indicator_on")
    protected String maintenanceIndicatorOn;
    @XmlElement(name = "no_signal")
    protected String noSignal;
    @XmlElement(name = "lightning_sensor_off")
    protected String lightningSensorOff;
    @XmlElement(name = "freezing_rain_sensor_off")
    protected String freezingRainSensorOff;
    @XmlElement(name = "present_weather_sensor_off")
    protected String presentWeatherSensorOff;

    /**
     * Obtém o valor da propriedade corrected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrected() {
        return corrected;
    }

    /**
     * Define o valor da propriedade corrected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrected(String value) {
        this.corrected = value;
    }

    /**
     * Obtém o valor da propriedade auto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuto() {
        return auto;
    }

    /**
     * Define o valor da propriedade auto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuto(String value) {
        this.auto = value;
    }

    /**
     * Obtém o valor da propriedade autoStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoStation() {
        return autoStation;
    }

    /**
     * Define o valor da propriedade autoStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoStation(String value) {
        this.autoStation = value;
    }

    /**
     * Obtém o valor da propriedade maintenanceIndicatorOn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintenanceIndicatorOn() {
        return maintenanceIndicatorOn;
    }

    /**
     * Define o valor da propriedade maintenanceIndicatorOn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintenanceIndicatorOn(String value) {
        this.maintenanceIndicatorOn = value;
    }

    /**
     * Obtém o valor da propriedade noSignal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoSignal() {
        return noSignal;
    }

    /**
     * Define o valor da propriedade noSignal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoSignal(String value) {
        this.noSignal = value;
    }

    /**
     * Obtém o valor da propriedade lightningSensorOff.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLightningSensorOff() {
        return lightningSensorOff;
    }

    /**
     * Define o valor da propriedade lightningSensorOff.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLightningSensorOff(String value) {
        this.lightningSensorOff = value;
    }

    /**
     * Obtém o valor da propriedade freezingRainSensorOff.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreezingRainSensorOff() {
        return freezingRainSensorOff;
    }

    /**
     * Define o valor da propriedade freezingRainSensorOff.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreezingRainSensorOff(String value) {
        this.freezingRainSensorOff = value;
    }

    /**
     * Obtém o valor da propriedade presentWeatherSensorOff.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresentWeatherSensorOff() {
        return presentWeatherSensorOff;
    }

    /**
     * Define o valor da propriedade presentWeatherSensorOff.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresentWeatherSensorOff(String value) {
        this.presentWeatherSensorOff = value;
    }

}
