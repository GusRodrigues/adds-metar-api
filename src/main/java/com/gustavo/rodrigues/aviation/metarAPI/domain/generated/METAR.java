//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.07.29 às 05:09:49 PM EDT 
//


package com.gustavo.rodrigues.aviation.metarAPI.domain.generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}raw_text" minOccurs="0"/>
 *         &lt;element ref="{}station_id" minOccurs="0"/>
 *         &lt;element ref="{}longitude" minOccurs="0"/>
 *         &lt;element ref="{}temp_c" minOccurs="0"/>
 *         &lt;element ref="{}dewpoint_c" minOccurs="0"/>
 *         &lt;element ref="{}wind_dir_degrees" minOccurs="0"/>
 *         &lt;element ref="{}wind_speed_kt" minOccurs="0"/>
 *         &lt;element ref="{}wind_gust_kt" minOccurs="0"/>
 *         &lt;element ref="{}visibility_statute_mi" minOccurs="0"/>
 *         &lt;element ref="{}altim_in_hg" minOccurs="0"/>
 *         &lt;element ref="{}sea_level_pressure_mb" minOccurs="0"/>
 *         &lt;element ref="{}quality_control_flags" minOccurs="0"/>
 *         &lt;element ref="{}wx_string" minOccurs="0"/>
 *         &lt;element ref="{}sky_condition" maxOccurs="4" minOccurs="0"/>
 *         &lt;element ref="{}flight_category" minOccurs="0"/>
 *         &lt;element ref="{}three_hr_pressure_tendency_mb" minOccurs="0"/>
 *         &lt;element ref="{}maxT_c" minOccurs="0"/>
 *         &lt;element ref="{}minT_c" minOccurs="0"/>
 *         &lt;element ref="{}maxT24hr_c" minOccurs="0"/>
 *         &lt;element ref="{}minT24hr_c" minOccurs="0"/>
 *         &lt;element ref="{}precip_in" minOccurs="0"/>
 *         &lt;element ref="{}pcp3hr_in" minOccurs="0"/>
 *         &lt;element ref="{}pcp6hr_in" minOccurs="0"/>
 *         &lt;element ref="{}pcp24hr_in" minOccurs="0"/>
 *         &lt;element ref="{}snow_in" minOccurs="0"/>
 *         &lt;element ref="{}vert_vis_ft" minOccurs="0"/>
 *         &lt;element ref="{}metar_type" minOccurs="0"/>
 *         &lt;element ref="{}elevation_m" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rawText",
    "stationId",
    "longitude",
    "tempC",
    "dewpointC",
    "windDirDegrees",
    "windSpeedKt",
    "windGustKt",
    "visibilityStatuteMi",
    "altimInHg",
    "seaLevelPressureMb",
    "qualityControlFlags",
    "wxString",
    "skyCondition",
    "flightCategory",
    "threeHrPressureTendencyMb",
    "maxTC",
    "minTC",
    "maxT24HrC",
    "minT24HrC",
    "precipIn",
    "pcp3HrIn",
    "pcp6HrIn",
    "pcp24HrIn",
    "snowIn",
    "vertVisFt",
    "metarType",
    "elevationM"
})
@XmlRootElement(name = "METAR")
@JsonIgnoreProperties(ignoreUnknown = true)
public class METAR {

    @XmlElement(name = "raw_text")
    protected String rawText;
    @XmlElement(name = "station_id")
    protected String stationId;
    protected Float longitude;
    @XmlElement(name = "temp_c")
    protected Float tempC;
    @XmlElement(name = "dewpoint_c")
    protected Float dewpointC;
    @XmlElement(name = "wind_dir_degrees")
    protected Integer windDirDegrees;
    @XmlElement(name = "wind_speed_kt")
    protected Integer windSpeedKt;
    @XmlElement(name = "wind_gust_kt")
    protected Integer windGustKt;
    @XmlElement(name = "visibility_statute_mi")
    protected Float visibilityStatuteMi;
    @XmlElement(name = "altim_in_hg")
    protected Float altimInHg;
    @XmlElement(name = "sea_level_pressure_mb")
    protected Float seaLevelPressureMb;
    @XmlElement(name = "quality_control_flags")
    protected QualityControlFlags qualityControlFlags;
    @XmlElement(name = "wx_string")
    protected String wxString;
    @XmlElement(name = "sky_condition")
    protected List<SkyCondition> skyCondition;
    @XmlElement(name = "flight_category")
    protected String flightCategory;
    @XmlElement(name = "three_hr_pressure_tendency_mb")
    protected Float threeHrPressureTendencyMb;
    @XmlElement(name = "maxT_c")
    protected Float maxTC;
    @XmlElement(name = "minT_c")
    protected Float minTC;
    @XmlElement(name = "maxT24hr_c")
    protected Float maxT24HrC;
    @XmlElement(name = "minT24hr_c")
    protected Float minT24HrC;
    @XmlElement(name = "precip_in")
    protected Float precipIn;
    @XmlElement(name = "pcp3hr_in")
    protected Float pcp3HrIn;
    @XmlElement(name = "pcp6hr_in")
    protected Float pcp6HrIn;
    @XmlElement(name = "pcp24hr_in")
    protected Float pcp24HrIn;
    @XmlElement(name = "snow_in")
    protected Float snowIn;
    @XmlElement(name = "vert_vis_ft")
    protected Integer vertVisFt;
    @XmlElement(name = "metar_type")
    protected String metarType;
    @XmlElement(name = "elevation_m")
    protected Float elevationM;

    /**
     * Obtém o valor da propriedade rawText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRawText() {
        return rawText;
    }

    /**
     * Define o valor da propriedade rawText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRawText(String value) {
        this.rawText = value;
    }

    /**
     * Obtém o valor da propriedade stationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationId() {
        return stationId;
    }

    /**
     * Define o valor da propriedade stationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationId(String value) {
        this.stationId = value;
    }

    /**
     * Obtém o valor da propriedade longitude.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     * Define o valor da propriedade longitude.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLongitude(Float value) {
        this.longitude = value;
    }

    /**
     * Obtém o valor da propriedade tempC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTempC() {
        return tempC;
    }

    /**
     * Define o valor da propriedade tempC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTempC(Float value) {
        this.tempC = value;
    }

    /**
     * Obtém o valor da propriedade dewpointC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDewpointC() {
        return dewpointC;
    }

    /**
     * Define o valor da propriedade dewpointC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDewpointC(Float value) {
        this.dewpointC = value;
    }

    /**
     * Obtém o valor da propriedade windDirDegrees.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWindDirDegrees() {
        return windDirDegrees;
    }

    /**
     * Define o valor da propriedade windDirDegrees.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWindDirDegrees(Integer value) {
        this.windDirDegrees = value;
    }

    /**
     * Obtém o valor da propriedade windSpeedKt.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWindSpeedKt() {
        return windSpeedKt;
    }

    /**
     * Define o valor da propriedade windSpeedKt.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWindSpeedKt(Integer value) {
        this.windSpeedKt = value;
    }

    /**
     * Obtém o valor da propriedade windGustKt.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWindGustKt() {
        return windGustKt;
    }

    /**
     * Define o valor da propriedade windGustKt.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWindGustKt(Integer value) {
        this.windGustKt = value;
    }

    /**
     * Obtém o valor da propriedade visibilityStatuteMi.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getVisibilityStatuteMi() {
        return visibilityStatuteMi;
    }

    /**
     * Define o valor da propriedade visibilityStatuteMi.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setVisibilityStatuteMi(Float value) {
        this.visibilityStatuteMi = value;
    }

    /**
     * Obtém o valor da propriedade altimInHg.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getAltimInHg() {
        return altimInHg;
    }

    /**
     * Define o valor da propriedade altimInHg.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setAltimInHg(Float value) {
        this.altimInHg = value;
    }

    /**
     * Obtém o valor da propriedade seaLevelPressureMb.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getSeaLevelPressureMb() {
        return seaLevelPressureMb;
    }

    /**
     * Define o valor da propriedade seaLevelPressureMb.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setSeaLevelPressureMb(Float value) {
        this.seaLevelPressureMb = value;
    }

    /**
     * Obtém o valor da propriedade qualityControlFlags.
     * 
     * @return
     *     possible object is
     *     {@link QualityControlFlags }
     *     
     */
    public QualityControlFlags getQualityControlFlags() {
        return qualityControlFlags;
    }

    /**
     * Define o valor da propriedade qualityControlFlags.
     * 
     * @param value
     *     allowed object is
     *     {@link QualityControlFlags }
     *     
     */
    public void setQualityControlFlags(QualityControlFlags value) {
        this.qualityControlFlags = value;
    }

    /**
     * Obtém o valor da propriedade wxString.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWxString() {
        return wxString;
    }

    /**
     * Define o valor da propriedade wxString.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWxString(String value) {
        this.wxString = value;
    }

    /**
     * Gets the value of the skyCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skyCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkyCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkyCondition }
     * 
     * 
     */
    public List<SkyCondition> getSkyCondition() {
        if (skyCondition == null) {
            skyCondition = new ArrayList<SkyCondition>();
        }
        return this.skyCondition;
    }

    /**
     * Obtém o valor da propriedade flightCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightCategory() {
        return flightCategory;
    }

    /**
     * Define o valor da propriedade flightCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightCategory(String value) {
        this.flightCategory = value;
    }

    /**
     * Obtém o valor da propriedade threeHrPressureTendencyMb.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getThreeHrPressureTendencyMb() {
        return threeHrPressureTendencyMb;
    }

    /**
     * Define o valor da propriedade threeHrPressureTendencyMb.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setThreeHrPressureTendencyMb(Float value) {
        this.threeHrPressureTendencyMb = value;
    }

    /**
     * Obtém o valor da propriedade maxTC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMaxTC() {
        return maxTC;
    }

    /**
     * Define o valor da propriedade maxTC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMaxTC(Float value) {
        this.maxTC = value;
    }

    /**
     * Obtém o valor da propriedade minTC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMinTC() {
        return minTC;
    }

    /**
     * Define o valor da propriedade minTC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMinTC(Float value) {
        this.minTC = value;
    }

    /**
     * Obtém o valor da propriedade maxT24HrC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMaxT24HrC() {
        return maxT24HrC;
    }

    /**
     * Define o valor da propriedade maxT24HrC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMaxT24HrC(Float value) {
        this.maxT24HrC = value;
    }

    /**
     * Obtém o valor da propriedade minT24HrC.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMinT24HrC() {
        return minT24HrC;
    }

    /**
     * Define o valor da propriedade minT24HrC.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMinT24HrC(Float value) {
        this.minT24HrC = value;
    }

    /**
     * Obtém o valor da propriedade precipIn.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getPrecipIn() {
        return precipIn;
    }

    /**
     * Define o valor da propriedade precipIn.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPrecipIn(Float value) {
        this.precipIn = value;
    }

    /**
     * Obtém o valor da propriedade pcp3HrIn.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getPcp3HrIn() {
        return pcp3HrIn;
    }

    /**
     * Define o valor da propriedade pcp3HrIn.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPcp3HrIn(Float value) {
        this.pcp3HrIn = value;
    }

    /**
     * Obtém o valor da propriedade pcp6HrIn.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getPcp6HrIn() {
        return pcp6HrIn;
    }

    /**
     * Define o valor da propriedade pcp6HrIn.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPcp6HrIn(Float value) {
        this.pcp6HrIn = value;
    }

    /**
     * Obtém o valor da propriedade pcp24HrIn.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getPcp24HrIn() {
        return pcp24HrIn;
    }

    /**
     * Define o valor da propriedade pcp24HrIn.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPcp24HrIn(Float value) {
        this.pcp24HrIn = value;
    }

    /**
     * Obtém o valor da propriedade snowIn.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getSnowIn() {
        return snowIn;
    }

    /**
     * Define o valor da propriedade snowIn.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setSnowIn(Float value) {
        this.snowIn = value;
    }

    /**
     * Obtém o valor da propriedade vertVisFt.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVertVisFt() {
        return vertVisFt;
    }

    /**
     * Define o valor da propriedade vertVisFt.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVertVisFt(Integer value) {
        this.vertVisFt = value;
    }

    /**
     * Obtém o valor da propriedade metarType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetarType() {
        return metarType;
    }

    /**
     * Define o valor da propriedade metarType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetarType(String value) {
        this.metarType = value;
    }

    /**
     * Obtém o valor da propriedade elevationM.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getElevationM() {
        return elevationM;
    }

    /**
     * Define o valor da propriedade elevationM.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setElevationM(Float value) {
        this.elevationM = value;
    }

}
