package com.gustavo.rodrigues.aviation.metarAPI.domain;

public enum QueryBuilder {
	API_URL("https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requesttype=retrieve&format=xml"),
	MOST_RECENT_TIME("&hoursBeforeNow=1"), 
	MOST_RECENT_ONLY("&hoursBeforeNow=1&mostRecentForEachStation=constraint"),
	TIME_BEFORE("&hoursBeforeNow="), STATIONS_TO_QUERY("&stationString=");

	private String url;

	QueryBuilder(String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url;
	}

	@Override
	public String toString() {
		return this.url;
	}
}
