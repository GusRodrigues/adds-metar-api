package com.gustavo.rodrigues.aviation.metarAPI.domain.unit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.gustavo.rodrigues.aviation.metarAPI.domain.QueryBuilder;

public class QueryBuilderTest {
	@Test
	public void should() {
		String apiUrl = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requesttype=retrieve&format=xml";
		assertThat(QueryBuilder.API_URL.getUrl(),is(apiUrl));
		
		String mostRecent = "&hoursBeforeNow=1&mostRecentForEachStation=constraint";
		assertThat(QueryBuilder.MOST_RECENT_ONLY.getUrl(), is(mostRecent));

		String recentTime = "&hoursBeforeNow=1";
		assertThat(QueryBuilder.MOST_RECENT_TIME.getUrl(), is(recentTime));
		
		String timeBefore = "&hoursBeforeNow=";
		assertThat(QueryBuilder.TIME_BEFORE.getUrl(), is(timeBefore));
		
		String station = "&stationString=";
		assertThat(QueryBuilder.STATIONS_TO_QUERY.getUrl(), is(station));
		
		assertThat(timeBefore.equalsIgnoreCase(QueryBuilder.TIME_BEFORE.toString()), is(true));
		
	}
}
