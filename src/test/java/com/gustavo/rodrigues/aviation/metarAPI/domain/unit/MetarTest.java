package com.gustavo.rodrigues.aviation.metarAPI.domain.unit;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.gustavo.rodrigues.aviation.metarAPI.domain.Metar;

public class MetarTest {
	@Test(expected = NullPointerException.class)
	public void testNull() {
		// given null, expect NPE
		Metar.raw(null);
	}
	@Test(expected = NullPointerException.class)
	public void testEmptySpaced() {
		// given whitespaced, expect NPE
		Metar.raw("    ");
	}
	@Test
	public void testEmptyMetar() {
		Metar notEmpty = Metar.raw("empty");
		assertThat(notEmpty.isEmpty(), is(false));
		
		Metar alsoEmpty = Metar.empty();
		assertThat(alsoEmpty.isEmpty(), is(true));
		assertThat(alsoEmpty.equals(notEmpty), is(false));	
	}
	
	@Test
	public void testEquality() {

		//given a b and c...
		Metar caseA = Metar.raw("a");
		Metar caseB = Metar.raw("a");
		Metar caseC = Metar.raw("a");
		Metar different = Metar.raw("different");
		
		// a = a (reflexive)
		assertThat(caseA.equals(caseA), is(true));
		assertThat(caseA.hashCode() == caseA.hashCode(), is(true));
		assertThat(caseA.equals(different), is(false));
		assertThat(caseA.hashCode() == different.hashCode(), is(false));
		// a = b ; b = a (symmetric)
		assertThat(caseA.equals(caseB),is(true));
		assertThat(caseB.equals(caseA),is(true));
		assertThat(caseB.hashCode() == caseA.hashCode(), is(true));
		assertThat(caseA.hashCode() == caseB.hashCode(), is(true));
		assertThat(caseB.hashCode() == different.hashCode(), is(false));
		// a = b & b = c ; c = a (Transitive)
		assertThat(caseA.equals(caseB),is(true));
		assertThat(caseB.equals(caseC),is(true));
		assertThat(caseC.equals(caseA),is(true));
		assertThat(caseA.hashCode() == caseB.hashCode(), is(true));
		assertThat(caseB.hashCode() == caseC.hashCode(), is(true));
		assertThat(caseC.hashCode() == caseA.hashCode(), is(true));
		assertThat(caseC.hashCode() == different.hashCode(), is(false));
		// null not equal. Since there is no null type on metar, no null hash necessary
		assertThat(caseA.equals(null),is(false));
		assertThat(null == caseA, is(false));
		// inequality
		assertThat(caseA.equals("string"), is(false));
	}
	
	@Test
	public void testMetarWithValidCase() {
		String test = "valid";
		Metar valid = Metar.raw(test);
		assertThat(valid.isEmpty(), is(false));
		assertThat(valid.getRawText().equalsIgnoreCase("__"), is(false));
		assertThat(valid.getRawText().equalsIgnoreCase(" "), is(false));
		assertThat(valid.getRawText().equalsIgnoreCase(test), is(true));
		assertThat(valid.toString().equalsIgnoreCase(test), is(true));
	}
	
	@Test
	public void testReportingStation() {
		Metar metar = Metar.raw("KATL teste metar");
		assertThat(metar.reportingStation().equalsIgnoreCase("katl"), is(true));
		assertThat(Metar.empty().reportingStation().equalsIgnoreCase("no one is reporting"), is(true));
		
	}
}
